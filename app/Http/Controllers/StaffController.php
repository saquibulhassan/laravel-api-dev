<?php

namespace App\Http\Controllers;

use App\Http\Resources\StaffResource;
use App\Model\Staff;
use Illuminate\Http\Request;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get staff
        $staff = Staff::paginate(15);

        dd($staff);

        // return collection of staff as a resource
        return StaffResource::collection($staff);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $staff = $request->isMethod('put') ? Staff::findOrFail($request->staff_id) : new Staff;

        $staff->id             = $request->input('staff_id');
        $staff->name           = $request->input('name');
        $staff->email          = $request->input('email');
        $staff->address        = $request->input('address');
        $staff->salary         = $request->input('salary');
        $staff->phone          = $request->input('phone');
        $staff->phone          = $request->input('phone');
        $staff->designation_id = $request->input('designation');
        $staff->department_id  = $request->input('department');

        if ($staff->save()) {
            return new StaffResource($staff);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Get single staff
        $staff = Staff::findOrFail($id);

        // return single staff as a resource
        return new StaffResource($staff);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Staff $staff
     * @return \Illuminate\Http\Response
     */
    public function edit(Staff $staff)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Model\Staff $staff
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Staff $staff)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Staff $staff
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Get single staff
        $staff = Staff::findOrFail($id);

        if ($staff->delete()) {
            return new StaffResource($staff);
        }
    }
}
