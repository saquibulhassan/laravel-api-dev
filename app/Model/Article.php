<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public function user()
    {
        //return $this->belongsTo('App\Model\User', 'user_id');
        return $this->belongsTo('App\Model\User');
        //return $this->hasOne('App\Model\User', 'id', 'user_id');
    }
}
