<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    public function department()
    {
        return $this->belongsTo('App\Model\Department');
    }

    public function designation()
    {
        return $this->belongsTo('App\Model\Designation');
    }
}
