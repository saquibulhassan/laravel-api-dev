@foreach($articles as $article)
    <h2>{{ $article->title }} <small style="font-style: italic">Posted by {{ $article->user->name }}</small></h2>
    <p>{{ $article->body }}</p>
@endforeach