<h1>{{ $user->name }}'s Profile</h1>

<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad assumenda delectus deserunt earum error exercitationem in inventore mollitia officia omnis provident quae quisquam, rem, saepe, totam vero voluptatibus! Cupiditate, repellat.</p>

<p>Address: </p>
<p>{{ $user->address->line1 }} <br > {{ $user->address->line2 }}</p>
<p>{{ $user->address->city }}, {{ $user->address->country }}</p>

<hr>

@foreach($user->articles as $article)
    <h2>{{ $article->title }}</h2>
    <p>{{ $article->body }}</p>
@endforeach