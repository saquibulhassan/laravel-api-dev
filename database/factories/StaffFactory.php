<?php

use Faker\Generator as Faker;

$factory->define(App\Model\Staff::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
        'address' => $faker->address,
        'salary' => $faker->randomNumber(4),
        'designation_id' => $faker->numberBetween(1, 10),
        'department_id' => $faker->numberBetween(1, 5),
    ];
});
