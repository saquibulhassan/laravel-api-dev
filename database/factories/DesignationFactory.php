<?php

use Faker\Generator as Faker;

$factory->define(App\Model\Designation::class, function (Faker $faker) {
    return [
        'designation' => $faker->word()
    ];
});
