<?php

use Faker\Generator as Faker;

$factory->define(App\Model\Address::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(1, 3),
        'line1'   => $faker->streetAddress,
        'city'    => $faker->city,
        'country' => $faker->country,
    ];
});
