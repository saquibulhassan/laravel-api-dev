<?php

use Faker\Generator as Faker;

$factory->define(App\Model\Article::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(1, 3),
        'title' => $faker->text(200),
        'body' => $faker->text()
    ];
});
