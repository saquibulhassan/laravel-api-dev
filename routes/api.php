<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// list staff
Route::get('staff', 'StaffController@index');

// list single staff
Route::get('staff/{id}', 'StaffController@show');

// create new staff
Route::post('staff', 'StaffController@store');

// update staff
Route::put('staff', 'StaffController@store');

// delete staff
Route::delete('staff/{id}', 'StaffController@destroy');
